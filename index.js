var {app_router,app}=require("./src/route/route");
var user_router=require("./src/route/user")
var proc_router=require("./src/route/proc")
var admin_router=require("./src/route/admin")
var router = require("./src/route/image")

const { date } = require("@hapi/joi");
const { patch } = require("./src/route/proc");
const port = 3001 || process.env.PORT;



app.use('/api', app_router)
app.use('/user', user_router)
app.use('/project',proc_router)
app.use('/admin',admin_router)
app.use('/image',router)

//new line
//url for bad request it means enter url not found 
app.use((req,res,next)=>{
    res.status(404).json({
        massage:"Page not Found,URL not Found/ Bad URL"
    })
    next();
})

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})