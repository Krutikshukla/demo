
const Joi = require('@hapi/joi');

const schema = Joi.object(
    {
        email:Joi.string().required()
              .email({ 
                    minDomainSegments: 2,
                    tlds: { allow: ['com', 'net']} 
                  }),
        password:[
                  Joi.string().required(),
                  Joi.number()
                 ]
                 
});
module.exports=schema