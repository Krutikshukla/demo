var express = require("express");
const bcrypt = require("bcrypt");
var pool = require("./../db/conn");
var nodemailer = require('nodemailer');
//import {v4 as uuid4} from 'uuid';

require("dotenv").config();


var app = express();
var admin_router = express.Router();
admin_router.use(express.json());


//http://localhost:3000/admin/register
admin_router.post("/register", (req, res) => {

    let f_nm = req.body.first_name;
    let l_nm = req.body.last_name;
    let c_nm = req.body.company_name;
  //  let city = req.body.city;
    let p_no = req.body.phone;
    let email = req.body.email;
  //  let rol_id = req.body.role_id;
 //   let i_active=req.body.is_active;
    let password = req.body.password;
    let cpassword = req.body.cpassword;
    //const {error} = abc(req.body)
    console.log(req.body)
    if (cpassword == password) {

         let select_query = 'SELECT * FROM `user` WHERE `email`=?';
         pool.query(select_query, [email], (err, result) => {
             if (result.length > 0) {
                 res.status(409).send({
                     StatusCode: res.statusCode,
                     message: "User already exists..."
                 });

             }
            else {

                let encryptpass = bcrypt.hashSync(password, 10)
                console.log('Encription password:',encryptpass)
                let query = `insert into user(first_name,last_name,company_name,phone,email,password) VALUES(?,?,?,?,?,?)`;
               
                pool.query(query, [f_nm,l_nm,c_nm,p_no,email,encryptpass], (err, rows) => {
                    if (err) throw err;
                    console.log("Row inserted with id = " + rows.insertId);
                    
                    var transporter = nodemailer.createTransport({
                        host: 'smtp.gmail.com',
                        port: 587,
                        secure: false, // secure:true for port 465, secure:false for port 587
                        auth: {
                          user:'krutikshukla19@gmail.com',
                          pass:process.env.password
                        }
                      });
                    var mailOptions = {
                        from:'krutikshukla19@gmail.com',
                        to: email,
                        //'krutikshukla19@gmail.com',
                        subject: 'One Click ChatBot',
                        text: 'The registration been sucessfully done!',
                        html: `<h1>One Click ChatBOt ThankYou For REgistrations!</h1>`
                    //html: `<h1>hello </h1>`
                      };
                      transporter.sendMail(mailOptions, function(error, info){
                        if (error) {
                          console.log(error);
                        } else {
                          console.log('Email sent: ' + info.response);
                        }
                     });

                    res.status(200).json({
                        StatusCode: res.statusCode,
                        message: {
                            first_name:f_nm,
                            last_name:l_nm,
                            company_name:c_nm,
                            phone:p_no,
                            email:email,
                            password:password
                        }

                    // let query1 ="INSERT INTO `ChatBotColorTheme`(`user_id`, `chatThemeColor`, `headerText`, `chatbody`, `msgbox`, `inputBoxText`, `requestText`, `responseText`, `response`) VALUES (?,'red','red','red','red','red','red','red','red')"
                    // pool.query1(query1,[user_id],(err,rows)=>{
                    //         if(err) throw err;
                    //         console.log("insert",rows.insertId)
                    //         res.send('color record set...')
                    //     });
                        
                 });                
                    
                });
             }
         })

    } else {
        res.status(401).send({
            StatusCode: res.statusCode,
            message: "password and confirm password do not match"
        });

    }

});



//http://localhost:3000/admin/alldata/4
admin_router.get("/alldata/:user_id",(req,res,result)=>{
    pool.query("select first_name,last_name,company_name,phone,email,role_id from user where user_id=?", [req.params.user_id], (err, result) => {
        if (err) {
            return console.log("", err);
        }
        else {
            if (result.length > 0) {
                console.log(result)
                res.send({
                    StatusCode: res.statusCode = 200,
                    message: result        
                    
                })
            } else {
                res.send({
                    StatusCode: res.statusCode = 400,
                    message: "no data.."
                })
            }
        }
    });

});



//http://localhost:3000/admin/updateuser/4
admin_router.put("/updateuser/:user_id",(req,res,result)=>{  


    var first_name=req.body.first_name;
    var last_name = req.body.last_name;
    var email = req.body.email;
    var phone = req.body.phone;
    var company_name = req.body.company_name;
    let user_id= req.params.user_id;
    console.log(req.body)
    // let encryptpass1 = bcrypt.hashSync(oldpas, 10)
    //         console.log("Encrytion form oldpaswd :",oldpas,encryptpass1)
    // let query1 = `select * from user where user_id=?`
    // pool.query(query1,[user_id], (err, result) => {
    
        let query="update user set first_name=?,last_name=?,email=?,phone=?,company_name=? where user_id=?"
    
        pool.query(query,[first_name,last_name,email,phone,company_name,user_id],(err,result)=>{
            if(err) throw err;
            console.log("rows",result.affectedRows);
            res.status(200).json({

                message:"Record Updated SucessFully"

            })    
        });
 });



// http://localhost:3000/admin/(user_id)
 admin_router.get("/abc/:user_id", (req, res) => {
    // console.log("Fetching user with id:", req.params.id)
    pool.query("select first_name,last_name,email,company_name,phone from user where user_id=?", [req.params.user_id], (err, result) => {
        if (err) {
            return console.log("", err);
        }
        else {
            if (result.length > 0) {
                console.log(result)
                res.send({
                    StatusCode: res.statusCode = 200,
                    message: result
            
                })
            } else {
                res.send({
                    StatusCode: res.statusCode = 400,
                    message: "not match .."
                })
            }
        }

    });
}); 



//  http://localhost:3000/admin/(email)
//  admin_router.get("/emailfetch/:email", (req, res) => {
//     // console.log("Fetching user with id:", req.params.id)
//     pool.query("select first_name,last_name,email,company_name,phone from user where email=?", [req.params.email], (err, result) => {
//         if (err) {
//             return console.log("", err);
//         }
//         else {
//             if (result.length > 0) {
//                 console.log(result)
//                 res.send({
//                     StatusCode: res.statusCode = 200,
//                     message: result
            
//                 })
//             } else {
//                 res.send({
//                     StatusCode: res.statusCode = 400,
//                     message: "not match id.."
//                 })
//             }
//         }

//     });
// });



// //  http://localhost:3000/admin/count
// admin_router.get("/",(req, res) => {
//     // console.log("Fetching user with id:", req.params.id)
//     pool.query(`SELECT COUNT(user_id) from user;`,(err,result) => {
//         if (err) {
//             return console.log("", err);
//         }
//         else {
//             // if (result.length > 0) {
//             //     console.log(result)
//                 res.send({
//                     StatusCode: res.statusCode = 200,
//                     message: result
//                 })
//            };
//        //  }
//       });
// });      

//  http://localhost:3000/admin/userdata

admin_router.get("/alluser",(req, res) => {
    // console.log("Fetching user with id:", req.params.id)

    pool.query("select * from user",(err,result) => {
        if (err) throw err
            // if (result.length > 0) {
            //     console.log(result)
                res.send({
                    StatusCode: res.statusCode = 200,
                    message: result
                });
           
       //  }
      });
    })

//  http://localhost:3000/admin/count
admin_router.get("/",(req, res) => {
    // console.log("Fetching user with id:", req.params.id)
    pool.query(`SELECT COUNT(user_id) as umberofuser FROM user;`,(err,result) => {
        if (err) {
            return console.log("", err);
        }
        else {
            // if (result.length > 0) {
                //  console.log(result.toJSON())
                //  result  = JSON.parse(JSON.stringify(result))
                 res.status(200).send(result[0])
           };
       //  }
      });
}); 



//localhost:3000/admin/user/(:id)
admin_router.delete("/user/:id", (req, res) => {

    let user_id = req.params.id;
    let query = "delete from user WHERE user_id = ?";

    pool.query(query, user_id, (err, rows) => {
        if (err) throw err;

        console.log('Number of rows deleted = ' + rows.affectedRows);
        res.send({
            StatusCode: res.statusCode = 200,
            message: "User record Deleted...."
        });
    });
});


//http://localhost:3000/admin/changepas/6   
admin_router.put("/changepas/:user_id", (req, res) => {

    var oldpas=req.body.oldpas;
    var password = req.body.password;
    var cpassword = req.body.cpassword;
    let user_id = req.params.user_id;
    console.log(req.body)
    // let encryptpass1 = bcrypt.hashSync(oldpas, 10)
    //         console.log("Encrytion form oldpaswd :",oldpas,encryptpass1)
    let query1 = `select * from user where user_id=?`
    pool.query(query1,[user_id], (err, result) => {
        
        if (bcrypt.compareSync(oldpas,result[0].password))
        {
                if(password == cpassword){
                // let query1 = `select * from admin where user_id=?`
                let query = `update user set password=? where user_id=?`;
                
                let encryptpass = bcrypt.hashSync(password, 10)
                console.log("Encrytion form :",encryptpass)
                console.log("User id :",user_id)
               
                pool.query(query,[encryptpass,user_id], (err, result) => {
                   if (err) throw err;
                     console.log('affected..',result.affectedRows);
                     res.status(200).json({
                        message:"Your Password Updated SucessFully!,Thank you!"
                     });
                  });
        }
               else
               {
                   res.status(400).json({
                       message:"PassWord and Compare Password Don't Match,Please TrY Again!"
                    });
               }
               
            }
     else
       {
        res.status(400).json({
            message:"PassWord And Old Password Is Not Match,Please Try Again!"
         });
       }
       });
   
});


module.exports=admin_router;