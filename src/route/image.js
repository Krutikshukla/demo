const router = require('express').Router();
const cloudinary = require("../utils/cloudinary")
const upload = require("../utils/multer")
const pool = require("../db/conn");
const user_router = require('./user');
//const { image } = require('../utils/cloudinary');


//http://localhost:3000/image/imginsert
router.post("/imginsert",upload.single("question"),async(req,res)=>{
    try {
        const result = await cloudinary.uploader.upload(req.file.path);
        let user_id=req.body.user_id;
       // let image=req.body.image;
        // let type=req.body.type;
        let answer=req.body.answer;
       // let question=req.body.question;
        let question=result.secure_url;
        let answers=req.body.answers;

         console.log("CloudInary_LINK Geneated: ",question)
        let query="INSERT INTO `QA`(`user_id`,`answer`, `question`, `answers`) VALUES (?,?,?,?)"
    //     // let query="INSERT INTO `QA`(`user_id`, `answers`) VALUES (?,?)"
    //    // let query="INSERT INTO `image`(`user_id`, `image`, `link`) VALUES (?,?,?)"
         pool.query(query,[user_id,answer,question,answers],(err,rows)=>
        {
       if(err) throw err;
            else{
            res.json({
               // message:"inert"
                user_id:user_id,
                 answer:answer,
                 question:result.secure_url,
                 answers:answers
                 // cloudinaryId:result.public_id
              })
            }
        });
        

    } catch (error) {
        console.log(error)
        return
    }
});



//http://localhost:3000/image/(:user_id)
router.get("/:user_id",(req,res)=>{
    try {
        let user_id=req.params.user_id;
        // const result = await cloudinary.uploader.upload(req.file.path);
        // let user_id=req.body.user_id;
        // let image=req.body.image;
        // let link=result.secure_url;
        // console.log(link)
        let query="select * from image where user_id=?"
        pool.query(query,[req.params.user_id],(err,rows)=>
        {
            if(err) throw err;
            else{
            res.json(rows)
           }
        });
    } catch (error) {
        console.log(error)

    }
});

//http://localhost:3000/image/(:user_id)
router.put("/:user_id",upload.single("link"),async(req,res)=>{
    
        const result1 =await cloudinary.uploader.upload(req.file.path);
        let user_id=req.params.user_id;
        //let image=req.body.image;
        let link=result1.secure_url;
        console.log("CloudInary_LINK Geneated: ",link)

        let query="update image set link=? where user_id=?"

        pool.query(query,[link,user_id],(err,rows,result)=>
        {
            if (err) throw err

            console.log("No Of Rows Effected: ",rows.affectedRows)
            res.status(200).json({
                  message:"Your,Image been Updated SuccesFully!"
                  //user_id:user_id,
                  //image:image,
                  //link:result.secure_url
                  //cloudinaryId:result.public_id
             });
        });
});  

//http://localhost:3000/image/(:user_id)
router.delete("/:user_id",(req,res)=>{
    try {
        let user_id=req.params.user_id;
        // const result = await cloudinary.uploader.upload(req.file.path);
        // let user_id=req.body.user_id;
        // let image=req.body.image;
        // let link=result.secure_url;
        // console.log(link)
        let query="delete from image where user_id=?"
        pool.query(query,[user_id],(err,rows)=>
        {
            if(err) throw err;
            console.log("Number of Rows affected:",rows.affectedRows);
            
            res.status(200).json({
                message:"Your Data Photo is been deleted,Succesfully Thank you"
            })
           
        });
    } catch (error) {
        console.log(error)

    }
});

module.exports =router