
  var express = require('express');
  const req = require('express/lib/request');
  const res = require('express/lib/response');
  var app = express();
  const multer = require('multer');
  const path = require('path')
  var pool = require("./../db/conn");
  const {v4 : uuidv4} = require('uuid')

  var proc_router = express.Router()

  proc_router.use(express.json());

  const storage = multer.diskStorage({
    destination:'./src/upload',
    filename:(req,file,cb)=>
    {
        return cb(null,`${file.fieldname}_${Date.now()}_${file.buffer}${path.extname(file.originalname)}`)
    }
});

const upload = multer({
    storage:storage
})



    proc_router.get("/ans/:id",(req,res,result)=>{
    pool.query("select ans_id,answer from aswer where ques_id=?", [req.params.id], (err, result) => {
        if (err) {
            return console.log("", err);
        }
        else {
            if (result.length > 0) {
                console.log(result)
                res.send({
                    StatusCode: res.statusCode = 200,
                    message: result
                    
                    
                })
            } else {
                res.send({
                    StatusCode: res.statusCode = 400,
                    message: "no data.."
                })
            }
        }
    });

    });



    proc_router.get("/ques/:id",(req,res,result)=>{
        pool.query("select q_id,questions from question_table where answerq_id=?", [req.params.id], (err, result) => {
            if (err) {
                return console.log("", err);
            }
            else {
                if (result.length > 0) {
                    console.log(result)
                    res.send({
                        StatusCode: res.statusCode = 200,
                        message: result
                        
                        
                    })
                } else {
                    res.send({
                        StatusCode: res.statusCode = 400,
                        message: "no data.."
                    })
                }
            }
        });
    });
 

    //inserting question and anwers new table
    //http://localhost:3000/project/qainsert
    proc_router.post("/qainsert",(req,res)=>{
        
        let user_id=req.body.user_id;
        let answer=req.body.answer;
        let question=req.body.question;
        let answers=req.body.answers;
     
        let select_query = 'SELECT * FROM `QA` WHERE `question`=? and `user_id`=?' ;
      
       
        pool.query(select_query, [question,user_id], (err, result) => {
    
               if (result.length > 0) {
                res.status(409).send({
                    //StatusCode: res.statusCode,
                    message: "Question is already exists try something else"
                });
       }
        else
       { 
        let query = `INSERT INTO QA(user_id, answer, question, answers) VALUES (?,?,?,?)`
            pool.query(query, [user_id,answer,question,answers], (err, rows) => {
                if (err) throw err;
                console.log("Row inserted with id = " + rows.insertId);
                res.send({
                    StatusCode: res.statusCode = 200,
                    message: "QUESTION AND ANWERS ARE BEEN INSERTED..."
                })   
            });
        }
    });
    });


    
    //http://localhost:3000/project/qainsert
    proc_router.post("/image12",upload.single('image'),(req,res)=>{
        
    //    let id=req.body.id;
        let image=req.file.filename;
        
        
        let query = "INSERT INTO `image1`(`iamge`) VALUES (?)"
            pool.query(query,image, (err, rows) => {
                if (err) throw err;
                //console.log("Row inserted with id = " + rows.insertId);
                res.send({
                    StatusCode: res.statusCode = 200,
                    message: "ARE BEEN INSERTED..."
                });   
            });        
    });



    proc_router.get("/image13/:id",(req,res,result)=>{

        pool.query("SELECT `iamge` FROM `image1` WHERE id=?", [req.params.id], (err, result) => {
            if (err) {
                return console.log("", err);
            }
            else {
                if (result.length > 0) {
                    console.log(result)
                    res.send({
                        StatusCode: res.statusCode = 200,
                        message: result
                        
                        
                    })
                } else {
                    res.send({
                        StatusCode: res.statusCode = 400,
                        message: "no data.."
                    })
                }
            }
        });
    });
    //fetching all details new tables//
    



    //http://localhost:3000/project/fetchqa/4
    proc_router.get("/fetchqa/:user_id",(req,res,result)=>{
        pool.query("select * from QA where user_id=?", [req.params.user_id], (err, result) => {
            if (err) {
                return console.log("", err);
            }
            else {
                if (result.length > 0) {
                    console.log(result)
                    res.send({
                        StatusCode: res.statusCode = 200,
                        message: result           
                    })
                } else {
                    res.send({
                        StatusCode: res.statusCode = 400,
                        message: "no data.."
                    })
                }
            }
        });
    });


    //fetching details new tables//
    //http://localhost:3000/project/quesfetch/where you wana go//
    proc_router.get("/quesfetch/:questions",(req,res,result)=>{
        pool.query("select answers from QA where question=?", [req.params.questions], (err, result) => {
            if (err) {
                return console.log("", err);
            }
            else {
                if (result.length > 0) {
                    console.log(result)
                    res.send({
                        StatusCode: res.statusCode = 200,
                        message: result
                        
                        
                    })
                } else {
                    res.send({
                        StatusCode: res.statusCode = 400,
                        message: "no data.."
                    })
                }
            }
        });
    });


    
     //question and answer API and user id
     proc_router.get("/demo/:user_id/:answer",(req,res,result)=>{
        console.log('asdsadsd')
        let ans=req.params.answer
        let user_id=req.params.user_id
        
        pool.query("SELECT question,answers from QA WHERE user_id=? AND answer=?", [user_id,ans] ,(err, result) => {
           
            if (err) {
                return console.log("error", err);
            }
            else {
                console.log(result)
                if (result.length > 0) {
                   
                    res.send({
                        StatusCode: res.statusCode = 200,
                        message: result
                        
                      //this api for demo  
                    })
                } else {
                    res.send({
                        StatusCode: res.statusCode = 400,
                        message: "no data.."
                    })
                }
            }
        });

        
     });

        
     //question LIST API
     //http://localhost:3000/project/question/1
     proc_router.get("/question/:user_id",(req, res) => 
    {
        const user_id = uuidv4()
        let id=req.params.user_id

    pool.query("select question from QA WHERE user_id=?", [id],(err, result) => 
    {
        if (err) 
        {
            console.log(err);
        }
        else {
            console.log(result)
            if (result.length > 0) {
               
                res.send({
                    StatusCode: res.statusCode = 200,
                    message: result  
                })
            } else {
                res.send({
                    StatusCode: res.statusCode = 400,
                    message: "There no Question and Answer Available,Please create Question and Answer sheet.."
                })
            }
        }
     });
 });

      
      //updating question and answers through question
      //http://localhost:3000/project/qaupdate/43
      proc_router.put("/qaupdate/:id",(req,res)=>{

        var answer=req.body.answer;
        var question = req.body.question;
        var answers = req.body.answers;
        // var phone = req.body.phone;
        // var company_name = req.body.company_name;
        let id= req.params.id;
        console.log(req.body)
        // let encryptpass1 = bcrypt.hashSync(oldpas, 10)
        //         console.log("Encrytion form oldpaswd :",oldpas,encryptpass1)
        // let query1 = `select * from user where user_id=?`
        // pool.query(query1,[user_id], (err, result) => {
        
            let query="update QA set answer=?,question=?,answers=? where id=?"
        
            pool.query(query,[answer,question,answers,id],(err,result)=>{
                if(err) throw err;
                console.log("rows",result.affectedRows);
                res.status(200).send('RECORD UPDATED SUCCESFULLY....');
            });
  
      });



       //delete answer answere where question?
      //http://localhost:3000/project/questiondel/44
      proc_router.delete("/questiondel/:id", (req, res) => {
   
    
        let query = "delete from QA where id=?";
        let id= req.params.id; 
    
        pool.query(query,id,(err, rows,result) => {
            if (err) throw err;
    
       //     res.send('Number of rows deleted = ' + rows.affectedRows)
            console.log('Number of rows deleted = ' + rows.affectedRows);
            console.log('question that is been deleted:',id)
            res.send({
                StatusCode: res.statusCode = 200,
                message: "User record Deleted SuscessFully"
            });
        });
      });

    
     //Null first question API
     proc_router.get("/abc",(req, res) => 
    {
    pool.query("select question,ans_arr from demo2 WHERE user_id=1 AND answer = 'null'", (err, result) => 
    {
        if (err) 
        {
            console.log(err);
        }
        else
        {
            res.send({
                   
                message: result
             });      
        }
        
     });
     });


     //http://localhost:3000/project/colorthemeinsert
     proc_router.post("/colorthemeinsert",(req,res)=>{

        let user_id=req.body.user_id;
        let chatThemeColor=req.body.chatThemeColor;	
        let headerText=req.body.headerText;
        let chatbody=req.body.chatbody;
        let msgbox=req.body.msgbox;
        let inputBoxText=req.body.inputBoxText;
        let requestText	=req.body.requestText;
        let responseText=req.body.responseText;
        let response=req.body.response
        let title=req.body.title;
        let positionTop=req.body.positionTop;
        let positionSide=req.body.positionSide;

        let select_query = `SELECT * FROM ChatBotColorTheme WHERE user_id=?` ;
      
       
        pool.query(select_query, [user_id], (err, result) => {
           if (result.length > 0) 
           {
                res.send({
                    StatusCode: res.statusCode=409,
                    message: "already exists try something else"
                });
            }
        else
     {
        let query =`INSERT INTO ChatBotColorTheme(user_id,chatThemeColor,headerText,chatbody,msgbox,inputBoxText,requestText,responseText,response,title,positionTop,positionSide) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)`
            pool.query(query,[user_id,chatThemeColor,headerText,chatbody,msgbox,inputBoxText,requestText,responseText,response,title,positionTop,positionSide],(err,rows)=>{

                if (err) throw err;
                console.log("Row inserted with id = " + rows.insertId);
                res.send({
                    StatusCode: res.statusCode = 200,
                    message: "inserted suscessfuly..."
                });   
            });
     }
         });
     });


     //http://localhost:3000/project/updatetheme/49
     proc_router.put("/updatetheme/:user_id",(req,res)=>{
        
        let chatThemeColor=req.body.chatThemeColor;	
        let headerText=req.body.headerText;
        let chatbody=req.body.chatbody;
        let msgbox=req.body.msgbox;
        let inputBoxText=req.body.inputBoxText;
        let requestText	=req.body.requestText;
        let responseText=req.body.responseText;
        let response=req.body.response
        let title=req.body.title;
        let positionTop=req.body.positionTop;
        let positionSide=req.body.positionSide;
        
        let user_id=req.params.user_id

        let query=`UPDATE ChatBotColorTheme SET chatThemeColor=?,headerText=?,chatbody=?,msgbox=?,inputBoxText=?,requestText=?,responseText=?,response=?,title=?,positionTop=?,positionSide=? WHERE user_id=?`

        pool.query(query,[chatThemeColor,headerText,chatbody,msgbox,inputBoxText,requestText,responseText,response,title,positionTop,positionSide,user_id],(err,result)=>{
            if(err) throw err;
            console.log("rows effected",result.affectedRows);
            res.status(200).send('record updated');
    
        });
     });


     //http://localhost:3000/project/updatestatus/19
     proc_router.put("/updatestatus/:id",(req,res)=>{
        let is_active=req.body.is_active;	
        
        let id=req.params.id

        let query=`UPDATE ChatBotColorTheme SET is_active=? WHERE id=?`

        pool.query(query,[is_active,id],(err,result)=>{
            if(err) throw err;
            console.log("rows effected",result.affectedRows);
            res.status(200).send('record updated');
    
        });
     });


     //http://localhost:3000/project/delcolor/47
     proc_router.delete("/delcolor/:id",(req,res)=>{

        let query=`DELETE FROM ChatBotColorTheme WHERE id=?`
        let id= req.params.id; 
        pool.query(query,id,(err, rows,result) => {
            if (err) throw err;
    
       //     res.send('Number of rows deleted = ' + rows.affectedRows)
            console.log('Number of rows deleted = ' + rows.affectedRows);
            console.log('question that is been deleted:',id)
            res.send({
                StatusCode: res.statusCode = 200,
                message: "User record Deleted SuscessFully"
            });
        });
     });



     //http://localhost:3000/project/color/46
     // ChatB otColorTheme//fetch all color theme by from user id
     proc_router.get("/color/:user_id",(req,res,result)=>{
   
         //console.log('asdsadsd')
       let user_id=req.params.user_id
       pool.query("SELECT id,chatThemeColor,is_active FROM `ChatBotColorTheme` WHERE user_id=?", [user_id] ,(err, result) => {
       
        if (err) {
            return console.log("error", err);
        }
        else {
            console.log(result)
            if (result.length > 0) {
               
                res.send({
                    StatusCode: res.statusCode = 200,
                    message: result
                    
                  //this api for demo  
                })
            } else {
                res.send({
                    StatusCode: res.statusCode = 400,
                    message: "no data.."
                })
            }
        }
       });

     });

    
     
 module.exports=proc_router
