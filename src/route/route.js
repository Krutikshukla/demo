var express = require('express');
const req = require('express/lib/request');
const res = require('express/lib/response');
var pool = require('../db/conn'); //connection pool
const check_auth = require('../middleware/check_auth');
const  bcrypt = require('bcrypt');

var app = express();

var app_router = express.Router()
app_router.use(express.json());

//localhost:8001/api/user
//token allow for this path
//check authentication and checking path
app_router.get("/user",check_auth,(req, res) => {
    pool.query("select  COUNT(user_id) as total from user", (err, result) => {
        console.log(err)
        if (err) {
            res.send(
                {
                    StatusCode: res.statusCode = 400,
                    message: "No Fetch Data... "
                }
            )

        }
        else {
            let count = result[0].total;//find total record
            if (0 < count) {
                pool.query("select first_name,last_name,company_name,phone from user", (err, result2) => {
                    if (err) {
                        res.send(
                            {
                                StatusCode: res.statusCode = 400,
                                message: "Not Fetch User Data... "
                            }
                              )
                    }
                    else {
                        res.send(result2)
                    }
                })
            }
            else {
                res.send(
                    {
                        StatusCode: res.statusCode = 400,
                        message: "No user Record available..."
                    }
                )
            }
        }
    });
});


//login
//localhost:3000/api/user/login/
app_router.post("/user/login",check_auth,(req, res) => {
    let email = req.body.email;
    let password = req.body.password;

    pool.query("select first_name,last_name,company_name,phone from user where email=? and password=?", [email, password],
        (err, result) => {
            if (err) {
                console.log(err)
            } else {
                if (result.length > 0) {

                    res.send(
                        {
                            StatusCode: res.statusCode = 200,
                            message: "Authentication User..."
                        }
                    )
                } else {
                    res.send({
                        StatusCode: res.statusCode = 400,
                        message: "no user found"
                    })
                }
            }
        });



});


//get one  user by id
//localhost:8001/api/user/2
app_router.get("/user/:id", (req, res) => {
    // console.log("Fetching user with id:", req.params.id)
    pool.query("select * from user where user_id=?", [req.params.id], (err, result) => {
        if (err) {
            return console.log("", err);
        }
        else {
            if (result.length > 0) {
                console.log(result)
                res.send({
                    StatusCode: res.statusCode = 200,
                    message: result
            
                })
            } else {
                res.send({
                    StatusCode: res.statusCode = 400,
                    message: "not match id.."
                })
            }
        }

    });
})


//for practice purpose
app_router.post("/user",async (req, res) => {

    let f_nm = req.body.first_name;
    let l_nm = req.body.last_name;
    let city = req.body.city;
    let p_no = req.body.phone_no;
    let email = req.body.email_id;
    let rol_id = req.body.role_id;
    let password = req.body.password;
    let cpassword = req.body.cpassword;

   
        if (cpassword == password) {
            let query = `insert into admin(first_name, last_name,city, phone_no, email_id,rol_id, password) VALUES(?,?,?,?,?,?,?)`;
    
            pool.query(query, [f_nm, l_nm, city, p_no, email,rol_id, password], (err, rows) => {
                if (err) throw err;
                console.log("Row inserted with id = " + rows.insertId);
                res.send({
                    StatusCode: res.statusCode = 201,
                    message: "User Value accepted..."
                })
                
            });
        } else {
    
            res.send({
                StatusCode: res.statusCode = 400,
                message: "password and confirm password do not match"
            });
        }
   
    
});

//delete by user id  Query
//localhost:3000/api/user/2
app_router.delete("/userid/:id", (req, res) => {

    let query = "delete from user WHERE user_id = ?";
    let user_id = req.params.id;

    pool.query(query, user_id, (err, rows) => {
        if (err) throw err;

        console.log('Number of rows deleted = ' + rows.affectedRows);
        res.send({
            StatusCode: res.statusCode = 200,
            message: "User record Deleted...."
        });
    });
});


//delete by user email id  Query 
app_router.delete("/useremail/:email", (req, res) => {
   
    
    let query = "delete from user where email=?";
    let email = req.params.email; 

    pool.query(query,email,(err, rows,result) => {
        if (err) throw err;

   //     res.send('Number of rows deleted = ' + rows.affectedRows)
        console.log('Number of rows deleted = ' + rows.affectedRows);
        console.log('Email that is been deleted:',email)
        res.send({
            StatusCode: res.statusCode = 200,
            message: "User record Deleted SuscessFully"
        });
    });
});

//update user
app_router.put("/user/:id", (req, res) => {
    var fir_name = req.body.first_name;
    var las_name = req.body.last_name;
    var com_name = req.body.company_name;
    var phone_no = req.body.phone;
    var email_id = req.body.email;
  //  var password = req.body.password;
    console.log(req.body)
    // let query1 = `select * from admin where user_id=?`
    let query = `update user set first_name=?,last_name=?,company_name=?,phone=?,email=? where user_id=?`;
   // let query1 = `update user set update_at where user_id=?`;
    let user_id = req.params.id;
  //  let encryptpass = bcrypt.hashSync(password, 10)
  //  console.log(encryptpass)
    console.log(user_id)
    // pool.query(query1,[fir_name,las_name,com_name, phone_no, email_id,encryptpass,user_id], (err, result) => {
    //     if (err) throw err;
    //     console.log('affected..',result.affectedRows);
    //     res.send('Record Updated..');
    // pool.query1(query1,[update_at,user_id],(err,res)=>{
    //     if(err) throw err;
    //     else{
    //         res.send('your date and time is also been updated')
    //     }
    // });
    pool.query(query,[fir_name,las_name,com_name, phone_no, email_id,user_id], (err, result) => {
        if (err) throw err;
        console.log('affected..',result.affectedRows); 
        res.send('Record Updated..');
       
    });
});

module.exports = {app_router,app};

