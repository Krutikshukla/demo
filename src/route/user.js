var express = require("express");
const bcrypt = require("bcrypt");
var pool = require("./../db/conn");
var nodemailer = require('nodemailer');
const Joi = require('@hapi/joi');
const {v4 : uuidv4} = require('uuid')
const schema = require('../middleware/validation')
const newId = uuidv4()


var jwt = require("jsonwebtoken");
const res = require("express/lib/response");
require("dotenv").config();

var app = express();
var user_router = express.Router();

user_router.use(express.json());


// /user/registration/
//http://localhost:3000/user/registration
user_router.post("/registration", (req, res) => {

    let f_nm = req.body.first_name;
    let l_nm = req.body.last_name;
    let c_nm = req.body.company_name;
  //  let city = req.body.city;
    let p_no = req.body.phone;
    let email = req.body.email;
  //  let rol_id = req.body.role_id;
 //   let i_active=req.body.is_active;
    let password = req.body.password;
    let cpassword = req.body.cpassword;
    //const {error} = abc(req.body)

    // let result=schema.validate(req.body)
            
    //  if(result.error)
    //  {
    //     res.status(400).send({message: result.error.details[0].message})
    //         return ; 
    //  }
    console.log(req.body)
    if (cpassword == password) {

         let select_query = 'SELECT * FROM `user` WHERE `email`=?';
         pool.query(select_query, [email], (err, result) => {
             if (result.length > 0) {
                 res.status(409).send({
                     StatusCode: res.statusCode,
                     message: "User already exists..."
                 });

             }
            else {

                let encryptpass = bcrypt.hashSync(password, 10)
                console.log('Encription password:',encryptpass)
                let query = `insert into user(first_name,last_name,company_name,phone,email,password) VALUES(?,?,?,?,?,?)`;
                const Encptedid = uuidv4()
                pool.query(query, [f_nm,l_nm,c_nm,p_no,email,encryptpass], (err, rows) => {
                    if (err) throw err;
                    console.log("Row inserted with id = " + rows.insertId);
                    
                    var transporter = nodemailer.createTransport({
                        host: 'smtp.gmail.com',
                        port: 587,
                        secure: false, // secure:true for port 465, secure:false for port 587
                        auth: {
                          user:'krutikshukla19@gmail.com',
                          pass:process.env.password
                        }
                      });
                    var mailOptions = {
                        from:'krutikshukla19@gmail.com',
                        to: 'krutikshukla19@gmail.com',
                        //'krutikshukla19@gmail.com',
                        subject: 'Sending Email using Node.js',
                        text: 'The registration been sucessfully done!',
                        html: `<h1>One Click ChatBOt ThankYou For REgistrations!</h1>`
                    //html: `<h1>hello </h1>`
                      };
                      transporter.sendMail(mailOptions, function(error, info){
                        if (error) {
                          console.log(error);
                        } else {
                          console.log('Email sent: ' + info.response);
                        }
                     });

                    res.status(200).json({
                        StatusCode: res.statusCode,
                        message: {
                           // Encptedid,
                            //user_id:user_id,
                            first_name:f_nm,
                            last_name:l_nm,
                            company_name:c_nm,
                            phone:p_no,
                            email:email,
                            password:password
                        }

                    // let query1 ="INSERT INTO `ChatBotColorTheme`(`user_id`, `chatThemeColor`, `headerText`, `chatbody`, `msgbox`, `inputBoxText`, `requestText`, `responseText`, `response`) VALUES (?,'red','red','red','red','red','red','red','red')"
                    // pool.query1(query1,[user_id],(err,rows)=>{
                    //         if(err) throw err;
                    //         console.log("insert",rows.insertId)
                    //         res.send('color record set...')
                    //     });
                        
                 });
                   
                   
                   
                        
                    
                });
             }
         })

    } else {
        res.status(401).send({
            StatusCode: res.statusCode,
            message: "password and confirm password do not match"
        });

    }

});



//user login :user/login
user_router.post("/login", (req, res) => {
    let email = req.body.email;
    let password = req.body.password;
    const Encptedid = uuidv4()
    // const schema = Joi.object({
    //              email: Joi.string().required()
    //              .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net']} }),
    //               password:Joi.string().required()
    //          });  
        
             let result=schema.validate(req.body)
            
             if(result.error){
                res.status(400).send({message: result.error.details[0].message})
                 return ;  }
        
    let query = "SELECT * FROM `user` WHERE email=? AND is_active=0";

    
    pool.query(query, [email], (err, result) => {
        try{
     
                    if (bcrypt.compareSync(password, result[0].password)) {
                        //create token 1.payload,2.secret key,3.time
                       
                        const token = jwt.sign({ 
        
                            first_name: result[0].first_name,
                            phone: result[0].phone,
                            email: result[0].email
                        }, 
                        process.env.SECRET_KEY,//secret key
                        {
                            expiresIn: "1 days"
                        });
                        
        
                        res.status(200).send({
                            Encptedid,
                            user_id:result[0].user_id,
                            role_id:result[0].role_id,
                            email: result[0].email,
                            token:token,
                            StatusCode: res.statusCode,
                            message: "Authenticated User...and Generate token.."
                        });
                    }
                    else {
                        res.status(401).send({
                            StatusCode: res.statusCode,
                            message: "Not Authenticated User ...Password matching fail.."
                        });
        
                    }
            

        }catch{
            res.status(404).send({
                StatusCode: res.statusCode,
                message: "User Not Exist.."
            });
        }
        

    })
});


//user login :user/login
//http://localhost:3000/user/login
// user_router.post("/login",(req, res,next) => {
//     let email = req.body.email; 
//     let password = req.body.password;
     
//      const schema = Joi.object({
//          email: Joi.string().required()
//          .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net']} }),
//           password:Joi.string().required()
//      });  

//      let result=schema.validate(req.body)
    
//      if(result.error){
//         res.status(400).send({message: result.error.details[0].message})
//          return ;  }

//       let query = "SELECT * FROM `user` WHERE email=? AND password=?";
    
//     pool.query(query, [email], (err, result) => {
//         try{
          
//                     if (bcrypt.compareSync(password, result[0].password)) {
//                         //create token 1.payload,2.secret key,3.time
                        
                                                           
//                         const token = jwt.sign({ 
        
//                             first_name: result[0].first_name,
//                             phone: result[0].phone,
//                             email: result[0].email
//                         }, 
//                         process.env.SECRET_KEY,//secret key
//                         {
//                             expiresIn: "1 days"
//                         });

//                         res.status(200).send({
//                             user_id:result[0].user_id,
//                             role_id:result[0].role_id,
//                             email: result[0].email,
//                             token:token,
//                             StatusCode: res.statusCode,
//                             message: "Authenticated User...and Generate token.."
//                         });
//                     }
//                     else {
//                         res.status(401).send({
//                             StatusCode: res.statusCode,
//                             message: "Not Authenticated User ...Password matching fail.."
//                         });
        
//                     }
//         }catch{
//             res.status(404).send({
//                 StatusCode: res.statusCode,
//                 message: "User Not Exist.."
//             });
//         }
        

//     })
// });
   



// user_router.put("/forgetpassword",(req,res)=>{

//     let email = req.body.email; 

//     let select_query = 'SELECT * FROM `user` WHERE `email`=?';
//     pool.query(select_query, [email], (err, result) => {
//         //if (result.length > 0) {
//             // res.status(409).send({
//             //     StatusCode: res.statusCode,
//             //     message: "User already exists..."
//             // });

//         //    let encryptpass = bcrypt.hashSync(password, 10)
//         //    console.log('Encription password:',encryptpass)
//            const token = jwt.sign({ 
        
//             first_name: result[0].first_name,
//             phone: result[0].phone,
//             email: result[0].email
//           }, 
//           process.env.FORGETPAS, //secret key
//           {
//             expiresIn: "20m"
//           });
           
//           var transporter = nodemailer.createTransport({
//             host: 'smtp.gmail.com',
//             port: 587,
//             secure: false, // secure:true for port 465, secure:false for port 587
//             auth: {
//               user:'krutikshukla19@gmail.com',
//               pass:process.env.password
//             }
//           });
//         var mailOptions = {
//             from:'krutikshukla19@gmail.com',
//             to: 'krutikshukla19@gmail.com',
//             //'krutikshukla19@gmail.com',
//             subject: 'MEESAAGE TO RESET THE PASSWORD',
//             text: 'The registration been sucessfully done!',
//             html: `<h1>Please click on the given link to reset the password</h1>
//                  <p>${process.env.SECRET_KEY}/user/login/${token}"</p>`
//         //html: `<h1>hello </h1>`
//           };
//           transporter.sendMail(mailOptions, function(error, info){
//             if (error) {
//               console.log(error);
//             } else {
//               console.log('Email sent: ' + info.response);
//             }
//          });
//          res.send({
//              message:'Email is been sent to the your account please check!'
//          })
//     });
// });

//http://localhost:3000/user/fnln/65
//api for selecting name and las namae
user_router.get("/fnln/:user_id",(req,res,result)=>{
    pool.query("select first_name,last_name from user where user_id=?", [req.params.user_id], (err, result) => {
        if (err) {
            return console.log("", err);
        }
        else {
            if (result.length > 0) {
                console.log(result)
                res.send({
                    StatusCode: res.statusCode = 200,
                    message: result        
                    
                })
            } else {
                res.send({
                    StatusCode: res.statusCode = 400,
                    message: "no data.."
                })
            }
        }
    });

});



//api to fetch all data 
//http://localhost:3000/user/alldata/4
user_router.get("/alldata/:user_id",(req,res,result)=>{
    pool.query("select first_name,last_name,company_name,phone,email,role_id from user where user_id=?", [req.params.user_id], (err, result) => {
        if (err) {
            return console.log("", err);
        }
        else {
            if (result.length > 0) {
                console.log(result)
                res.send({
                    StatusCode: res.statusCode = 200,
                    message: result        
                    
                })
            } else {
                res.send({
                    StatusCode: res.statusCode = 400,
                    message: "no data.."
                })
            }
        }
    });

});


//update particular user
//http://localhost:3000/user/updateuser/4
user_router.put("/updateuser/:user_id",(req,res,result)=>{  


var first_name=req.body.first_name;
var last_name = req.body.last_name;
var email = req.body.email;
var phone = req.body.phone;
var company_name = req.body.company_name;
let user_id= req.params.user_id;
console.log(req.body)
// let encryptpass1 = bcrypt.hashSync(oldpas, 10)
//         console.log("Encrytion form oldpaswd :",oldpas,encryptpass1)
// let query1 = `select * from user where user_id=?`
// pool.query(query1,[user_id], (err, result) => {

    let query="update user set first_name=?,last_name=?,email=?,phone=?,company_name=? where user_id=?"

    pool.query(query,[first_name,last_name,email,phone,company_name,user_id],(err,result)=>{
        if(err) throw err;
        console.log("rows",result.affectedRows);
        res.send('record updated succesfuly');

    });
});



//changing password are ther fro new pas
//http://localhost:3000/user/changepas/6   
user_router.put("/changepas/:user_id", (req, res) => {

    var oldpas=req.body.oldpas;
    var password = req.body.password;
    var cpassword = req.body.cpassword;
    let user_id = req.params.user_id;
    console.log(req.body)
    // let encryptpass1 = bcrypt.hashSync(oldpas, 10)
    //         console.log("Encrytion form oldpaswd :",oldpas,encryptpass1)
    let query1 = `select * from user where user_id=?`
    pool.query(query1,[user_id], (err, result) => {
        
        if (bcrypt.compareSync(oldpas,result[0].password))
        {
                if(password == cpassword){
                // let query1 = `select * from admin where user_id=?`
                let query = `update user set password=? where user_id=?`;
                
                let encryptpass = bcrypt.hashSync(password, 10)
                console.log("Encrytion form :",encryptpass)
                console.log("User id :",user_id)
               
                pool.query(query,[encryptpass,user_id], (err, result) => {
                   if (err) throw err;
                     console.log('affected..',result.affectedRows);
                    res.send('Record Updated..');
                  });
        }
               else
               {
                   res.send('password and compare pasword dont match');
               }
               
            }
     else
       {
           res.send('your password doest match with the old one');
       }
       });
   
});



module.exports = user_router;



